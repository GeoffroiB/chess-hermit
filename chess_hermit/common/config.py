import json


def read_config():
    with open('config.json', 'r', encoding='utf-8') as jsonFile:
        return json.load(jsonFile)