from sys import argv
from argparse import ArgumentParser


def parse(args=None):
    args = args or argv[:]
    parser = ArgumentParser()
    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('--train', help='train model in headless mode', action='store_true')
    action.add_argument('--watch', help='watch model play against itself', action='store_true')

    return vars(parser.parse_args())