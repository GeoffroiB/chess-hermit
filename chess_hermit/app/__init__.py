import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import sys
from flask import Flask, render_template, request, Response
import chess
import chess.svg
import numpy as np
from concurrent.futures import ThreadPoolExecutor
import threading

import chess_hermit.core.self_play as self_play
import chess_hermit.core.train as train
from chess_hermit.common.config import read_config


started_backend = False
backend_thread = None 
board = self_play.board
app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    global backend_thread, started_backend
    if not started_backend:
        config = read_config()
        backend_thread = threading.Thread(target=self_play.watch_self_play, args=(config,))
        started_backend = True
        backend_thread.start()
    return (
    "<!DOCTYPE html>"
    "<html>"
        "<head>"
            "<title>Chess Hermit</title>"
            "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>"
        "</head>"
        "<body>"
            "<div id='container-board' style='width:600px;height:600px'>"
            "</div>"
        "</body>"
        "<script type=text/javascript>"
            "setInterval(function() {"
                "$.ajax({"
                    "url: '/get_board'," 
                    "success: function(result) {"
                        "$('#container-board').html(result);"
                    "}"
                "});"
            "}, 200);"
            "$(function() { "
                "$('#random-button').on('click', function() {"
                    "$.ajax({url:'/play_random_move'});"
                "});"
            "});"
        "</script>"
    "</html>"
)

@app.route('/get_board')
def get_board():
    return chess.svg.board(board=self_play.board)

def run(command):
    if command == "watch_self_play":
        app.run(debug=True)
    elif command == "train_self_play":
        config = read_config()
        batch_size = 20
        existing_count = train.count_existing([config['data_dir'] + '/*.tfrecords', config['data_dir'] + '/*.pgn'])
        self_play.self_play(config, batch_size=batch_size-existing_count, )
        while True:
            train.train(config)
            self_play.self_play(config, batch_size=batch_size)   
