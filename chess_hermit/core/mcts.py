import numpy as np

from chess_hermit.core.state import State
from chess_hermit.core.util import softcount, softmax

c_puct = 5.0
eps = 0.25
dirichlet_alpha = 0.3
n_vl = 5

def policy (node, T=0.0):
    return softcount(node.visit_counts, T=T)

class MCTS:
    def __init__ (self, model, state, num_threads=1):
        self.model = model
        self.state = state

        self.num_threads = num_threads
        self.threads = self.spawn_threads()

        self.root = self.expand_root()

    def spawn_threads (self):
        threads = [Searcher(self) for i in range(self.num_threads)]
        return threads

    def search (self, num_simulations):
        for simulation in range(num_simulations//self.num_threads):
            states = [thread.get(self.root) for thread in self.threads]

            observations = [state.observation() for state in states]
            observations = np.stack(observations)
            batch_logits, batch_values = self.model.infer({'image' : observations})

            for i, thread in enumerate(self.threads):
                thread.put(batch_logits[i], batch_values[i][0])

        return self.root

    def act (self, index):
        # Get action from node
        action = self.root.actions[index]
        
        # Look up new node and value
        child = self.root.get_child(index)
        value = self.root.mean_values[index]

        # Update root with child node
        self.root = child

        self.push_action(action)

        # Early exit if game is over
        if self.state.done():
            return self

        # If child node didn't exist we need to expand root 
        if child is None:
            self.root = self.expand_root()

        return self

    def expand_root (self):
        logits, value = self.model.infer({'image' : [self.state.observation()]})
        self.root_value = value[0][0]
        return self.expand(self.state.actions(), logits[0])

    def expand (self, actions, logits):
        # Legal actions in this state
        actions = sorted (
            actions,
            key=lambda action : logits[action],
            reverse=True
        )

        # Prior probabilities
        logits = logits[actions]
        probs = softmax(logits, T=1.0)

        # Dirichlet noise to priors
        alpha = np.zeros_like(probs)
        alpha[:] = dirichlet_alpha
 
        noise = np.random.dirichlet(alpha)
        probs = (1-eps)*probs + eps*noise

        # Make new node for this position
        node = Node(actions)
        node.policy_values[:] = probs

        return node

    def push_action(self, action):
        # Update game state with action
        self.state.push_action(action)

        # Push action to threads
        for thread in self.threads:
            thread.push_action(action)




class Searcher:
    def __init__(self, parent):
        self.parent = parent
        self.state = self.parent.state.copy()
        
        self.node = None
        self.index = None
        self.trajectory = None

    def get(self, node):
        # Keep track of path taken
        trajectory = []

        # Find new action to expand
        while True:
            child, index = self.select(node)
            trajectory.append((node, index))

            if child is None:
                break

            node = child

        # Store search state
        self.node = node
        self.index = index
        self.trajectory = trajectory

        return self.state

    def put(self, logits, value):
        # Get search state
        node = self.node
        index = self.index
        trajectory = self.trajectory

        # Back up value if terminal
        if self.state.done():
            return self.backup(trajectory, self.state.reward())

        # If child node doesn't exist
        if node.get_child(index) is None:
            # Create child
            child = self.parent.expand(self.state.actions(), logits)

            # Link child into tree
            node.set_child(index, child)

        # Back up value-estimate to root
        return self.backup(trajectory, value)

    def push_action(self, action):
        self.state.push_action(action)

    def select(self, node):
        # We're in a valid node so pick index
        index = node.puct_index()
        child = node.get_child(index)

        # Update statistics
        node.select(index)

        # Find action and update state
        action = node.actions[index]
        self.state.push_action(action)

        return child, index

    def backup(self, trajectory, value):
        while trajectory:
            action = self.state.pop_action()

            node, index = trajectory.pop()
            value = -value

            node.backup(index, value)
            assert node.actions[index] == action

class Node:
    __slots__ = ['actions', 'children', 'visit_counts', 'mean_values', 'policy_values']

    def __init__ (self, actions):
        num_actions = len(actions)
        
        # Tree pointers
        self.actions = actions
        self.children = {}
        
        # Node statistics
        self.visit_counts = np.zeros(num_actions, dtype=np.int32) # visit counts
        self.mean_values = np.zeros(num_actions, dtype=np.float32) # mean values
        self.policy_values = np.zeros(num_actions, dtype=np.float32) # policy values

    def first_zero_index (self):
        return np.argmin(self.visit_counts)

    def puct_index (self):
        # Calculate action values and upper confidence bound
        mean_values = self.mean_values
        upper_confidences = c_puct * self.policy_values * np.sqrt(self.visit_counts.sum()) / (1 + self.visit_counts)

        # index of maximal sum of pair Q and U
        return np.argmax(mean_values+upper_confidences)

    def get_child (self, index):
        return self.children.get(index, None)

    def set_child (self, index, child):
        self.children[index] = child
        return child

    def terminal (self):
        return len(self.actions) == 0

    def select (self, index):        
        # Update node statistics
        visit_count = self.visit_counts[index]
        mean_value = self.mean_values[index]

        W = visit_count*mean_value

        self.visit_counts[index] = visit_count + n_vl
        self.mean_values[index] = (W - n_vl)/(visit_count + n_vl)

    def backup (self, index, value):
        visit_count = self.visit_counts[index]
        mean_value = self.mean_values[index]

        W = visit_count*mean_value

        self.visit_counts[index] = visit_count - n_vl + 1
        self.mean_values[index] = (visit_count + n_vl + value) / (visit_count - n_vl + 1)

    def pv (self):
        return self.pv_recurse([])

    def pv_recurse (self, moves):
        if np.all(self.visit_counts == 0):
            return moves

        index = np.argmax(self.visit_counts)
        action = self.actions[index]

        moves.append(action)
        return self.children[index].pv_recurse(moves)