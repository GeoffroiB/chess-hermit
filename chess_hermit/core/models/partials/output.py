import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import numpy as np


def output_module (net, filters, training=False):
    """Final shared layer before two headed output."""

    # 1x1 -> batch norm -> relu
    net = tf.layers.conv2d (
        net, filters, (1, 1), (1, 1), padding='SAME', name='conv_1x1/conv'
    )
    net = tf.layers.batch_normalization(net, training=training, name='conv_1x1/norm')
    net = tf.nn.relu(net, name='conv_1x1/relu')

    return net

def output_policy (net, n_classes, training=False):
    """Policy head for the network."""
    branch = net

    # 1x1 -> flattening
    branch = tf.layers.conv2d (
        branch, n_classes//(8*8), (1, 1), (1, 1), padding='SAME', name='logits'
    )

    dims = np.prod(branch.shape.as_list()[1:])
    branch = tf.reshape(branch, [-1, dims], name='flattened_logits')

    return branch

def output_value (net, training=False):
    """Value head for the network."""
    branch = net

    # flattening -> dense of scalar output
    dims = np.prod(branch.shape.as_list()[1:])
    branch = tf.reshape(branch, [-1, dims], name='flattened')

    branch = tf.layers.dense(branch, 1, name='logits')
    branch = tf.nn.tanh(branch, name='logits/tanh')

    return branch