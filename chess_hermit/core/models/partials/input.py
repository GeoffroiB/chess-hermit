import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
from chess_hermit.common.config import read_config

config = read_config()


def input_module (net, filters, training=False):
    """First layer that interfaces directly with binary feature vectors."""
    # 7x7 -> batch norm -> relu
    net = tf.layers.conv2d(net, filters, (7, 7), (1, 1), padding='SAME', name='conv_7x7/conv')
    net = tf.layers.batch_normalization(net, training=training, name='conv_7x7/norm')
    net = tf.nn.relu(net, name='conv_7x7/relu')
    return net

# Placeholder input function section
def placeholder_dict (names, shapes, dtypes):
    placeholders = {}
    if names is None:
        return placeholders
    for name, shape, dtype in zip(names, shapes, dtypes):
        placeholders[name] = tf.compat.v1.placeholder (
            dtype=dtype, shape=(None,) + tuple(shape), name=name
        )
    return placeholders

def placeholder_input_fn (
        feature_names, feature_shapes, feature_dtypes,
        label_names=None, label_shapes=None, label_dtypes=None
):
    def input_fn ():
        features = placeholder_dict (
            feature_names,
            feature_shapes,
            feature_dtypes
        )

        labels = placeholder_dict (
            label_names,
            label_shapes,
            label_dtypes
        )

        return features, labels

    return input_fn

# TFRecords input function section
def parse_fn (example):
    example_features = {
        'feature' : tf.FixedLenFeature([], tf.string),
        'value' : tf.FixedLenFeature([1], tf.float32),
        'pi_value' : tf.VarLenFeature(tf.float32),
        'pi_index' : tf.VarLenFeature(tf.int64)
    }

    example = tf.parse_single_example(example, features=example_features)

    image = tf.decode_raw(example['feature'], tf.int8)
    image = tf.reshape(image, config["input_shape"], name='image_reshaped')

    pi_value = tf.sparse.to_dense(example['pi_value'])
    pi_index= tf.sparse.to_dense(example['pi_index'])

    policy = tf.sparse_to_dense (
        sparse_indices=pi_index,
        output_shape=[config["n_classes"]],
        sparse_values=pi_value,
        validate_indices=False
    )

    legal_mask = tf.sparse_to_dense (
        sparse_indices=pi_index,
        output_shape=[config["n_classes"]],
        sparse_values=tf.zeros_like(pi_value),
        default_value=tf.float32.min,
        validate_indices=False
    )

    value = example['value']

    return image, legal_mask, value, policy

def dataset_input_fn (path, batch_size=32, num_epochs=1, buffer_size=2048):
    def input_fn():
        with tf.device('/cpu:0'):
            dataset = tf.data.TFRecordDataset(path, compression_type='GZIP')

            dataset = dataset.map(parse_fn)
            dataset = dataset.shuffle(buffer_size)
            dataset = dataset.repeat(num_epochs)
            dataset = dataset.batch(batch_size)

            iterator = dataset.make_one_shot_iterator()
            image, legal_mask, value, policy = iterator.get_next()

            return {'image' : image, 'legal_mask' : legal_mask}, {'value' : value, 'policy' : policy}
    return input_fn
