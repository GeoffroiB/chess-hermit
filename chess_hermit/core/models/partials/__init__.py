import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from chess_hermit.core.models.partials.input import input_module, dataset_input_fn, placeholder_input_fn
from chess_hermit.core.models.partials.residual import residual_module
from chess_hermit.core.models.partials.output import output_module, output_policy, output_value
from chess_hermit.core.util import AttrDict

import tensorflow as tf


def collection_getter (getter, *args, **kwargs):
    """Adds variables to relevant collections."""
    var = getter(*args, **kwargs)

    name = kwargs['name']
    trainable = kwargs['trainable']

    if trainable:
        if 'kernel' in name:
            tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.WEIGHTS, var)

        if 'bias' in name:
            tf.compat.v1.add_to_collection(tf.compat.v1.GraphKeys.BIASES, var)

    return var

def inference (inputs, filters, modules, n_classes, training=False):
    """Complete inference model."""
    net = inputs

    # Cast binary features into float32 (-> batch norm)
    with tf.variable_scope('transform'):
        net = tf.cast(net, tf.float32, name='image_float')
        net = tf.layers.batch_normalization(net, training=training, name='image_float/norm')
    # Run binary features through input module
    with tf.variable_scope('input'):
        net = input_module(net, filters, training=training)

    # Some amount of residual modules
    for layer in range(modules):
        with tf.variable_scope(f'residual_{layer}'):
            net = residual_module(net, training=training)

    # Output layer and policy, value heads
    with tf.variable_scope('output'):
        net = output_module(net, 8*8, training=training)
        with tf.variable_scope('policy'):
            policy = output_policy(net, n_classes, training=training)
        with tf.variable_scope('value'):
            value = output_value(net, training=training)

    return policy, value


def model_fn (features, labels, mode, params):
    # Training flag
    training = (mode == tf.estimator.ModeKeys.TRAIN)

    # Extract and concatenate features for input
    inputs = features['image']

    # Get unscaled log probabilities
    with tf.variable_scope (
            'inference',
            reuse=params.get('reuse', False)
            ,custom_getter=collection_getter
    ):
        policy, value = inference(
            inputs,
            filters=params['filters'],
            modules=params['modules'],
            n_classes=params['n_classes'],
            training=training
        )

    # Add summaries to weights
    for var in tf.compat.v1.trainable_variables():
        tf.compat.v1.summary.histogram(var.name.split(':')[0] + '_summary', var)

    # Specification
    spec = AttrDict (
        mode=mode,
        features=features,
        predictions=(policy, value)
    )

    # Return early inference specification
    if mode == tf.estimator.ModeKeys.PREDICT:
        return spec

    with tf.variable_scope('losses'):
        # Value loss
        value_loss = tf.compat.v1.losses.mean_squared_error (
            labels=labels['value'], predictions=value,
            weights=1.0/4.0
        )

        policy_loss = tf.compat.v1.losses.softmax_cross_entropy (
            onehot_labels=labels['policy'], logits=policy + features['legal_mask'],
            weights=1.0
        )

        # Get l2 regularization loss
        l2_loss = tf.contrib.layers.apply_regularization (
            tf.contrib.layers.l2_regularizer(params['l2_scale'])
        )
        tf.compat.v1.losses.add_loss(l2_loss)

        # Total loss
        loss = tf.compat.v1.losses.get_total_loss(add_regularization_losses=False)

        # Add total loss to loss collection
        tf.add_to_collection(tf.GraphKeys.LOSSES, loss)

    # Add summaries for losses
    for loss_tensor in tf.get_collection(tf.GraphKeys.LOSSES):
        tf.compat.v1.summary.scalar(loss_tensor.name.split(':')[0] + '_summary', loss_tensor)

    spec.labels = labels
    spec.loss = loss
    spec.eval_metric_ops = AttrDict()

    # Return early evaluation specification
    if mode == tf.estimator.ModeKeys.EVAL:
        return spec

    # Get global step for training op
    global_step = tf.compat.v1.train.get_global_step()

    with tf.variable_scope('train'):
        # Get optimizer function
        optimizer_fn = {
            'Adam' : tf.compat.v1.train.AdamOptimizer,
            'RMSProp' : tf.compat.v1.train.RMSPropOptimizer,
            'GradientDescent' : tf.compat.v1.train.GradientDescentOptimizer
        }[params.get('optimizer', 'Adam')]

        optimizer = optimizer_fn(params['learning_rate'])

        # Compute gradients and add summaries
        grads_and_tvars = optimizer.compute_gradients(spec.loss)

        # Create train operation
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            train_op = optimizer.apply_gradients (
                grads_and_tvars,
                global_step=global_step
            )

    # Add summaries for gradients
    with tf.variable_scope('gradients'):
        tf.contrib.training.add_gradients_summaries(grads_and_tvars)
    spec.train_op = train_op

    # Return full train specification
    return spec