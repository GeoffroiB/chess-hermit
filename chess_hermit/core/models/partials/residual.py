import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf


def residual_module (net, training=False):
    """Residual module, uses two 3x3 convs and post-activation."""
    filters = int(net.shape.as_list()[-1])

    # Along this branch 3x3 -> batch norm -> relu -> 3x3 -> batch norm
    with tf.variable_scope('branch'):
        branch = net
        branch = tf.layers.conv2d (
            branch, filters, (3, 3), (1, 1), padding='SAME', name='conv_3x3_0/conv'
        )
        branch = tf.layers.batch_normalization(branch, training=training, name='conv_3x3_0/norm')
        branch = tf.nn.relu(branch, name='conv_3x3_0/relu')

        branch = tf.layers.conv2d (
            branch, filters, (3, 3), (1, 1), padding='SAME', name='conv_3x3_1/conv'
        )
        branch = tf.layers.batch_normalization(branch, training=training, name='conv_3x3_1/norm')

    # Residual head, original input + branch -> batch norm -> relu
    with tf.variable_scope('residual'):
        net += branch
        net = tf.layers.batch_normalization(net, training=training, name='norm')
        net = tf.nn.relu(net, name='relu')

    return net