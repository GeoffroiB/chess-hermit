import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import tempfile
import glob
from chess_hermit.core.models.partials import model_fn, placeholder_input_fn
from chess_hermit.core.models.abstract import AbstractModel
from chess_hermit.common.config import read_config


config = read_config()


class PlayingModel(AbstractModel):
    def __init__(self):
        super().__init__()
        self.spec = self._build_playing_spec()

    def _build_playing_spec (self,graph=None):
        checkpoint_path = tf.train.latest_checkpoint(self.model_dir)

        if not checkpoint_path:
            tf.logging.warning(f'Found no trained model in {self.model_dir}')

        spec = self.build_graph(
            input_fn=placeholder_input_fn(
                feature_names=('image',),
                feature_shapes=(tuple(config['input_shape']),),
                feature_dtypes=(tf.int8,)
            )
            ,mode=tf.estimator.ModeKeys.PREDICT
            ,params={
                'filters' : config['filters'],
                'modules' : config['modules'],
                'n_classes' : config['n_classes']
            }
        )
        with spec.graph.as_default() as graph:
            global_step = tf.train.get_or_create_global_step()
            session = tf.compat.v1.train.MonitoredSession (
                session_creator=tf.compat.v1.train.ChiefSessionCreator(
                    checkpoint_dir=self.model_dir,
                    config=self.config
                )
            )
            spec.session = session
        spec.graph.finalize()
        return spec

    def feature_dict (self, features):
        return {
            self.spec.features[k] : features[k] for k in features
        }

    def infer (self, features):
        return self.spec.session.run (
            self.spec.predictions,
            feed_dict=self.feature_dict(features)
        )