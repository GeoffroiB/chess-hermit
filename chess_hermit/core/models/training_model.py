import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import tempfile
import glob
from chess_hermit.core.models.partials import model_fn, dataset_input_fn
from chess_hermit.core.models.abstract import AbstractModel
from chess_hermit.common.config import read_config


config = read_config()


class TrainingModel(AbstractModel):
    def __init__(self):
        super().__init__()
        self.spec = self._build_training_spec()

    def _build_training_spec(self,graph=None):
        inp = dataset_input_fn(
            path=glob.glob(config['data_dir']+"/*.tfrecords"),
            batch_size=config["batch_size"],
            num_epochs=config["num_epochs"],
            buffer_size=16384
        )
        spec = self.build_graph(
            input_fn=inp
            ,mode=tf.estimator.ModeKeys.TRAIN
            ,params={
                'filters' : config["filters"],
                'modules' : config["modules"],
                'n_classes' : config["n_classes"],
                'optimizer' : config["optimizer"],
                'learning_rate' : config["learning_rate"],
                'l2_scale' : config["l2_scale"]
            }
        )

        with spec.graph.as_default() as graph:
            session = tf.train.MonitoredTrainingSession (
                checkpoint_dir=config['model_dir'],
                config=self.config
            )
            spec.session = session
        spec.graph.finalize()
        return spec

    def train (self):
        loss, _ = self.spec.session.run(
            [self.spec.loss, self.spec.train_op]
        )
        return loss

    def should_stop (self):
        return self.spec.session.should_stop()