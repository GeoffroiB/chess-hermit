import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import tempfile
from chess_hermit.core.models.partials import model_fn
from chess_hermit.common.config import read_config

tf_config = tf.ConfigProto (
    device_count = {
        'CPU' : 1,
        'GPU' : 0
    },
    intra_op_parallelism_threads=1,
    inter_op_parallelism_threads=1,
    allow_soft_placement = True
)

config = read_config()

class AbstractModel:
    def __init__(self):
        self.model_fn = model_fn
        self.model_dir = config['model_dir']
        self.config = tf_config
        if self.model_dir is None:
            self.model_dir = tempfile.mkdtemp()
            tf.logging.warning('Using {} as model_dir'.format(self.model_dir))
        self.spec = None
        self.contexts = []

    def build_graph (self, input_fn, mode, params, graph=None):
        if graph is None:
            graph = tf.Graph()

        with graph.as_default():
            global_step = tf.train.get_or_create_global_step()

            with tf.device('/cpu:0'):
                with tf.compat.v1.variable_scope('input'):
                    features, labels = input_fn()

            spec = self.model_fn (
                features=features,
                labels=labels,
                mode=mode,
                params=params
            )
            spec.global_step = global_step

        spec.graph = graph
        return spec
    
    def __enter__ (self):
        # Add context managers
        self.contexts.append(self.spec.graph.as_default())
        self.contexts.append(self.spec.session)

        [ctx.__enter__() for ctx in self.contexts]
        return self

    def __exit__ (self, ex_type, ex_val, ex_trace):
        suppress = False

        # Pop contexts in reverse order as they were entered
        while self.contexts:
            ctx = self.contexts.pop()
            # Exit context
            ctx_suppress = ctx.__exit__(ex_type, ex_val, ex_trace)

            # Suppress if any of our managers decided to handle the error
            suppress = suppress or ctx_suppress

        return suppress