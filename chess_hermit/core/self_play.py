import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

import datetime
import os
import chess
import numpy as np
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import time
import sys

from chess_hermit.common.config import read_config
from chess_hermit.core.state import State
from chess_hermit.core.mcts import MCTS, policy
from chess_hermit.core.models.playing_model import PlayingModel
from chess_hermit.core.data_saving import write_game_records, write_pgn, write_records

board = None

def play_game (inference_model):
    global board
    actions,policies,indices,moves = [],[],[],[]
    
    # Set up search tree
    tree = MCTS(inference_model, State(), num_threads=8)

    # Play game
    while not tree.state.done():
        board = tree.state.state
        # Perform search
        node = tree.search(128)

        # Calculate move probabilities and get action index
        probs = policy(node, T=1.0)
        index = np.random.choice(len(node.actions), p=probs)

        # Get action and update tree
        action = node.actions[index]
        value = node.mean_values[index]
        move = tree.state.parse_action(action)

        tree.act(index)

        # Store stats
        actions.append(action)
        policies.append(probs)
        indices.append(node.actions)
        moves.append(move)

    # Get game outcome and last player to move
    outcome = -tree.state.reward()
    winner = not tree.state.turn()

    return actions, policies, indices, outcome, winner


def save_game(config,actions,policies,indices,outcome,winner):
    name = time.strftime("%Y%m%d-%H%M%S")
    moves = write_records(config['data_dir'], name, actions, policies, indices, outcome, winner)
    write_pgn(config['data_dir'], name, moves, outcome, winner)
    return None

def self_play_one_game(config, iteration):
    with PlayingModel() as inference_model:
        (actions,policies,indices,outcome,winner) = play_game(inference_model)
        save_game(config,actions,policies,indices,outcome,winner)
        print(f"[{datetime.datetime.now()}] completed game {iteration}")

def watch_self_play(config):
    with PlayingModel() as inference_model:
        while True:
            _ = play_game(inference_model)
    return 0

def self_play(config, batch_size=1):
    print(f"Starting batch of self-play of size {batch_size}")
    with ProcessPoolExecutor(max_workers=8) as executor:
        for i in range(batch_size):
            executor.submit(self_play_one_game, config, i)
    return 0