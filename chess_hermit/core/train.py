import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

import glob
import os
import numpy as np
import time
import datetime
import sys


from chess_hermit.core.models.training_model import TrainingModel
from chess_hermit.common.config import read_config


def cleanup(path):
    for f in glob.glob(path):
        os.remove(f)

def count_existing(paths):
    current_len = np.min([
        len(glob.glob(path))
        for path in paths
    ])
    return current_len

def train(config):
    losses = []
    if count_existing([config['data_dir'] + '/*.tfrecords', config['data_dir'] + '/*.pgn']) > 0:
        with  TrainingModel() as training:
            while not training.should_stop():
                loss = training.train()
                losses.append(loss)
            print(f"[{datetime.datetime.now()}] Average loss: {np.mean(losses)}")
        cleanup(config['data_dir'] + '/*.tfrecords')
        cleanup(config['data_dir'] + '/*.pgn')