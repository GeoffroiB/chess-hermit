import warnings
warnings.filterwarnings('ignore')
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import os
import time
import sys
import datetime
import tensorflow as tf
import chess

from chess_hermit.core.state import State


def create_features (feature, value, pi_value, pi_index):
    return tf.train.Features (feature={
        'feature' : tf.train.Feature(bytes_list=tf.train.BytesList(value=[feature.tostring()])),
        'value' : tf.train.Feature(float_list=tf.train.FloatList(value=[value])),
        'pi_value' :  tf.train.Feature(float_list=tf.train.FloatList(value=pi_value)),
        'pi_index' : tf.train.Feature(int64_list=tf.train.Int64List(value=pi_index))
    })

def convert_example (feature, value, pi_value, pi_index):
    return tf.train.Example(
        features=create_features(feature, value, pi_value, pi_index)
    )

def write_game_records(out_file, actions, policies, indices, outcome, winner):
    # Create new state
    state = State()
    moves = []

    # Run through game to create feature vectors and produce output
    for i, action in enumerate(actions):
        # Extract features
        feature = state.observation().reshape((1, 8, 8, -1))

        # Calculate value of game based on who's to play
        value = outcome if state.turn() == winner else -outcome

        # Write example to disk
        example = convert_example(feature, value, policies[i], indices[i])
        out_file.write(example.SerializeToString())

        # Update game state
        state.push_action(action)
        moves.append(state.state.peek())

    return moves

def write_records(data_dir, name, actions, policies, indices, outcome, winner):
    # Make directory for data if needed
    dirs = data_dir
    if not os.path.exists(dirs):
        os.makedirs(dirs)

    path = os.path.join(dirs, name) + '.tfrecords'

    # Open tfrecords file
    options = tf.io.TFRecordOptions(tf.compat.v1.python_io.TFRecordCompressionType.GZIP)
    with tf.io.TFRecordWriter(path, options=options) as out_file:
        moves = write_game_records(out_file, actions, policies, indices, outcome, winner)

    return moves

def write_pgn (pgn_dir, name, moves, outcome, winner):
    dirs = pgn_dir
    if not os.path.exists(dirs):
        os.makedirs(dirs)

    path = os.path.join(dirs, name) + '.pgn'
    pgn = [chess.Board().variation_san(moves)]

    if outcome:
        pgn.append('1-0' if winner == chess.WHITE else '0-1')
    else:
        pgn.append('1/2-1/2')

    with open(path, 'w') as out_file:
        print(' '.join(pgn), file=out_file)