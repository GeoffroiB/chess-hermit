import chess
import numpy as np

from chess_hermit.common.config import read_config
config = read_config()


def square_to_index (square):
    return np.unravel_index(square, config['input_shape'][:2])

def move_to_label (move):
    label = (
        square_to_index(move.from_square) +
        square_to_index(move.to_square)
    )

    return label

def move_to_label_flat (move):
    return np.ravel_multi_index(
        move_to_label(move),
        config['classes_shape']
    )

def index_to_square (idx):
    return chess.square(idx[1], idx[0])

def label_to_move (label):
    return chess.Move (
        index_to_square(label[0:2]),
        index_to_square(label[2:4])
    )

def label_flat_to_move (label):
    return label_to_move(
        np.unravel_index(label, config['classes_shape'])
    )

# Bitboard routines
def squares_to_bb (squares):
    indices = np.array([square_to_index(square) for square in squares])
    bb = np.zeros(config['input_shape'][:2], dtype=np.byte)
    try:
        if indices.shape[0]:
            bb[indices] = 1
    except IndexError:
        print(np.array(indices))
        raise IndexError

    return bb

def position_to_legal_bbs (position):
    legal_moves = list(position.legal_moves)
    indices = [move_to_label(move) for move in legal_moves]

    bbs = np.zeros(config['classes_shape'], dtype=np.byte)
    bbs[zip(*indices)] = 1

    return bbs

def bool_to_bb (boolean):
    allocator = np.ones if boolean else np.zeros
    return allocator(config['input_shape'][:2], dtype=np.byte)

def position_to_bool_bbs (position):
    colors = chess.COLORS

    to_play = [position.turn]
    is_check = [position.is_check()]
    kingside = [position.has_kingside_castling_rights(c) for c in colors]
    queenside = [position.has_queenside_castling_rights(c) for c in colors]

    bools = to_play + is_check + kingside + queenside

    bool_bbs = [bool_to_bb(b) for b  in bools]

    return np.array(bool_bbs)

def position_to_chw (position):
    occupancies = [
        position.pawns,   position.knights,
        position.bishops, position.rooks,
        position.queens,  position.kings
    ]

    # Full piece boards for each color
    occupancies = [chess.SquareSet(occ) for occ in occupancies]
    colors = [chess.SquareSet(occ) for occ  in position.occupied_co]

    # Color specific piece boards
    occupancies_black = [occ & colors[chess.BLACK] for occ in occupancies]
    occupancies_white = [occ & colors[chess.WHITE] for occ in occupancies]

    squaresets = occupancies + colors + occupancies_black + occupancies_white

    squares = [list(squares) for squares in squaresets]
    bbs = [squares_to_bb(square) for square in squares]
    bbs = np.array(bbs, dtype=np.byte)

    bool_bbs = position_to_bool_bbs(position)

    bbs_chw = np.concatenate (
        [
            bbs,
            bool_bbs
        ],
        axis = 0
    )

    return bbs_chw

def position_to_hwc (position):
    bbs_chw = position_to_chw(position) 
    return bbs_chw.transpose((1, 2, 0))
