import warnings
warnings.filterwarnings('ignore',category=FutureWarning)


from chess_hermit.app.parser import parse
from chess_hermit import app


if __name__ == "__main__":
    args = parse()
    if args["watch"]:
        app.run("watch_self_play")
    elif args["train"]:
        app.run("train_self_play")