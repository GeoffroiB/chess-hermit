# chess-hermit (originally b65-synthese)

## Description (FR)

Bien que la remise du projet aie déjà été faite, je continue à retravailler ce projet, afin qu'il reflète mon niveau d'expertise actuel.

## Description (ENG)

Even though this project has already been submitted and evaluated, I have continued to work on it, in order for it to reflect my current level of expertise.

## Instructions (FR)

À partir de la racine du projet,
	
	python -m chess_hermit --train : pour entrainer le modele
	
	python -m chess_hermit --watch : pour observer le modele en action
		lien pour observer : localhost:5000
	
	python -m tensorboard.main --logdir : pour observer la progression
		lien pour observer : localhost:6006

Avant utlisation, aller changer les 'paths' dans config.json et dans le  fichier 'checkpoint' dans model

## Instructions (ENG)
Todo